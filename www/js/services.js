angular.module('starter.services', ['config'])
    .service('databaseService', function () {
        var service = {}, userid, db;

        function createDB(tx) {
            console.log("Initialize...");
//            tx.executeSql('DROP TABLE IF EXISTS MESSAGES');
            tx.executeSql('CREATE TABLE IF NOT EXISTS MESSAGES (id integer primary key, message text, messagefrom text, messagetime text)');
        }

        // Transaction error callback
        //
        function errorCB(err) {
            console.log("Error opening DB: " + err.code);
        }

        // Transaction success callback
        //
        function successCB() {
            console.log("Database is open");
        }

        // Populate the database
        //
        function populateDB(message) {
            return function (tx) {
                console.log("Initialize...");
                console.dir(message);
                console.log('message  : ', JSON.stringify(message));
                console.log('Id       : ', message.from._id);
                console.log('timestamp: ', message.timestamp);
                try {
                    if (message.direction === 'received') {
                        tx.executeSql('INSERT INTO MESSAGES (message,messagefrom,messagetime) VALUES (?,?,?)', [JSON.stringify(message), message.from._id, message.timestamp]);
                    } else {
                        tx.executeSql('INSERT INTO MESSAGES (message,messagefrom,messagetime) VALUES (?,?,?)', [JSON.stringify(message), message.recipient._id, message.timestamp]);
                    }
                } catch (err) {
                    console.error('caught an error');
                    console.dir(err);
                }
            }
        }

        function populationSuccess() {
            console.log('Database properly populated');
        }

        function getMessages(number, fromid, success) {
            console.log("GetMessages...");
            console.dir(number);
            var result = [];
            return function (tx) {
                console.log('the from id is: ');
                console.log(fromid);
                try {
                    tx.executeSql("SELECT * FROM MESSAGES WHERE messagefrom = ? ORDER BY id DESC LIMIT ?;", [fromid, number]
                        , function (tx, rs) {
                            for (var i = 0; i < rs.rows.length; i++) {
                                var row = rs.rows.item(i)
                                result[i] = {message: row['message'], messagetime: row['messagetime']}
                            }
                            console.log(result);
                            success(result); // <-- new bit here
                        }, errorCB);
                } catch (err) {
                    console.error('caught an error in getMessages');
                    console.dir(err);
                }
            }
        }

        service.getNumberOfMessages = function (numberOfMessages, fromid, showMessageCB) {
            if (!!db) {
                console.log('about to start transaction');
                db.transaction(getMessages(numberOfMessages, fromid, showMessageCB));
            } else {
                console.log('Database not open');
            }
        };

        // message is added where it is from the userid sending
        // and the database written too
        service.addRow = function (message) {
            service.init(message.recipient._id);
            console.log('opening:', userid);
            console.dir(message);
            console.log(JSON.stringify(message));

            if (!!db) {
                db.transaction(populateDB(message), errorCB, populationSuccess);
            } else {
                console.log('Database not open');
            }
        };
        service.addSent = function (message) {
            service.init(userid);
            console.log(JSON.stringify(message));

            if (!!db) {
                db.transaction(populateDB(message), errorCB, populationSuccess);
            } else {
                console.log('Database not open');
            }
        };

        service.init = function (puserid) {
            userid = puserid;

            console.log("device ready...");
            db = window.openDatabase(userid, "1.0", "User Messages", 200000);
            db.transaction(createDB, errorCB, successCB);
        };
        return service;
    }
)
    .service('userService', ['$http', 'config_server', UserService]);

function UserService($http, config_server) {
    var service = {};
    service.getUser = function () {
        console.log("Tetting user: ");
        console.dir(service.user);
        return service.user;
    };

    service.login = function (puser, success) {
        $http.defaults.useXDomain = true;
        var req = {
            method: 'GET',
            url: config_server + '/api/users/login/' + puser.name + '/' + puser.password,
            headers: {
                'Content-Type': 'application/json'
            }
        };

        $http(req).then(
            function (res) {
                console.log("Result: ");
                console.dir(res);
                console.dir(res.data[0]);
                service.user = res.data[0];
                if (res.data instanceof Array) {
                    self.user = res.data[0];
                    success(res.data[0]);
                }
            }
            , function (err) {
                console.log("Error: ");
                console.log(err);
            }
        );
    };

    service.register = function (user, success, failure) {

        //        curl
        //        -H "Content-Type: application/json"
        //        -X POST -d '{"username":"xyz","email":"dsells@gmail.com","password":"xyz"}'
        //        http://192.168.0.109:3000/api/users

        var req = {
            method: 'POST',
            url: config_server + '/api/users',
            headers: {
                'Content-Type': 'application/json'
            },
            data: user
        };

        $http(req).then(
            function (res) {
                console.log("Result: " + res);
                success(res);
            }
            , function (err) {
                console.log("Error: " + err);
                failure(err);
            }
        );
    };

    service.updateUser = function (user) {

    };

    return service;
}
