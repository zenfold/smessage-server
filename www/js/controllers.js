angular.module('starter.controllers', ['config', 'btford.socket-io'])
    .factory('socket', function (socketFactory, config_server) {
        var myIoSocket = io.connect(config_server);
        mySocket = socketFactory({
            ioSocket: myIoSocket
        });
        return mySocket;
    })
    .service('messageService', function (databaseService) {
        var pendingMessages = {}, service = {};
        service.pushMessage = function (id, message) {
            databaseService.addRow(message);
        };
        service.setPendingForId = function (id, numberPending) {
            var messageCount = pendingMessages[id];
            if (messageCount === undefined) {
                pendingMessages[id] = numberPending;
            } else {
                pendingMessages[id] = messageCount + numberPending;
            }
        }

        service.numberOfMessages = function (id) {
            var messageCount = pendingMessages[id];
            var numberofmessages = 0;
            if (messageCount) {
                numberofmessages = messageCount;
            }
            return numberofmessages;
        };
        service.purgeMessageCount = function (id) {
            pendingMessages[id] = 0;
        };

        service.showCache = function () {
            console.log("-------------Message Cache ------------------");
            console.dir(pendingMessages);
        };
        return service;
    })


    .service('chatter', function (socket, $location, userService, messageService, databaseService) {
        var service = {}, users, activeUsers, currentUser, logoutSuccess, chatCB;
        service.initialize = function () {
            initialize();
        };
        service.getUserList = function (success) {
            getUserList(success);
        };
        service.getCurrentUser = function () {
            return getCurrentUser();
        };
        service.getUserListData = function (success) {
            getUserListData(success);
        };
        service.logout = function () {
            return logout();
        };
        service.numberOfUnread = function (id) {
            return numberOfUnread(id);
        };
        service.registerChatCB = function (chatCB) {
            return registerChatCB(chatCB);
        };

        function registerChatCB(pchatCB) {
            chatCB = pchatCB;
        }

        function getCurrentUser() {
            return currentUser;
        }

        function getUserListData(success) {
            success(users, activeUsers);
        }

        function initialize() {
            logoutSuccess = function () {
                $location.path('/#/log/login')
            };

            currentUser = userService.getUser();
            socket.emit('add user', userService.getUser());
        }

        function logout() {
            socket.emit('logout');
        }

        socket.on('loggedout', function () {
            logoutSuccess();
        });

        function numberOfUnread(id) {
            return messageService.numberOfMessages(id);
        }

        socket.on('pendingMessages', function (messages) {
            console.log('pendingMessages');
            angular.forEach(messages, function (message) {
                message.direction = 'received';
                messageService.pushMessage(message.from._id, message);
                messageService.setPendingForId(message.from._id, 1);
            });
            chatCB();
        });


        socket.on('new message', function (message) {
            if ($location.path() === '/tab/chats') {
                message.direction = 'received';
                messageService.pushMessage(message.from._id, message);
                messageService.setPendingForId(message.from._id, 1);
            }
        });

        socket.on('removed', function (info) {
            console.log('on(detail):removed');
            chatCB();
        });

        function getUserList(success) {
            console.log('emit(chatter):getUsers:', socket.id);
            socket.emit('getUsers');
            socket.on('userList', function (pusers) {
                console.log('on(chatter):userList');
                users = angular.fromJson(pusers);
                socket.emit('getLoggedonUsers');
                socket.on('userLoggedonList', function (pactiveUsers) {
                    console.log('on(chatter):userLoggedonList');
                    activeUsers = angular.fromJson(pactiveUsers);
                    success(users, activeUsers);
                });
            });
        }

        // -------------------- list control ---------------------------

        service.uniquePush = function (message, conversation) {
            var unique = true;
            angular.forEach(conversation, function (value) {
                if ((value.timestamp === message.timestamp) && (value.direction === message.direction)) {
                    unique = false;
                }
            });

            if (unique) {
                conversation.push(message);
            }

        };

        service.uniqueUnshift = function (message, conversation) {
            var unique = true;
            angular.forEach(conversation, function (value) {
                if ((value.timestamp === message.timestamp) && (value.direction === message.direction)) {
                    unique = false;
                }
            });

            if (unique) {
                conversation.unshift(message);
            }

        };

        service.send = function (message, conversation) {
            databaseService.addSent(message);
            service.uniqueUnshift(message, conversation);
            socket.emit('send', message);
        };

        var CB;
        service.registerCB = function (pCB) {
            CB = pCB;
        };

        socket.on('new message', function (message) {
            if ($location.path() === '/tab/chats') {
                return;
            }
            var messageCopy = angular.copy(message);
            messageCopy.direction = 'received';
            console.log('location: ' + $location.path());
            console.log('two push from new message');
            messageService.pushMessage(messageCopy.from._id, messageCopy);
            CB(messageCopy);
        });
        // -------------------------------------------------------------

        var userListCB;
        service.registerUserListCB = function (pUserListCB) {
            userListCB = pUserListCB;
        };

        socket.on('added', function (info) {
            console.log('on(detail):added:');
            if (userListCB) {
                userListCB();
            }
        });

        socket.on('removed', function (info) {
            console.log('on(detail):removed');
            if (userListCB) {
                userListCB();
            }
        });


        return service;
    }) // chatter end
    .controller('ChatsCtrl', function ($scope, $location, $rootScope, $state, chatter, databaseService) {
        console.log('Chat current state');
        console.log($location);
        console.dir($location);
        databaseService.init($rootScope.user._id);
        chatter.registerChatCB(function () {
            getUserList();
        });
        $scope.users = [];

        $scope.getUser = function () {
            return $rootScope.user;
        };

        $scope.numberOfUnread = function (id) {
            return chatter.numberOfUnread(id)
        };

        chatter.initialize();
        getUserList();
        function getUserList() {
            chatter.getUserList(function (usersr, activeUsers) {
                $scope.users = angular.fromJson(usersr);
                angular.forEach(activeUsers, function (user) {
                    angular.forEach(usersr, function (ib) {
                        if (ib._id === user._id) {
                            ib.socket_id = user.socket_id;
                        }
                    })
                });
            });
        }
    })

    .controller('ChatDetailCtrl', function ($scope, $stateParams, $location, $rootScope, chatter, messageService, databaseService) {
        var recipientid = $stateParams.chatId;
        console.log('Detail current state');
        console.log($location);
        console.dir($location);

        $scope.conversation = [];
        $scope.message = {};
        $scope.message.content = '';
        $scope.users = [];

        $scope.itemTracker = function (item) {
            return item.timestamp + item.direction;
        };

        $scope.logout = function () {
            chatter.logout();
        };

        $scope.recipientChanged = function(newRecipient) {
            console.log('---------- new Recipient ---------------');
            console.dir(newRecipient);
            getMessages(newRecipient._id,true);
        };

        var getUser = function () {
            return $rootScope.user;
        };

        function getById(a, id) {
            return a.filter(
                function (a) {
                    return a._id == id
                }
            );
        }

        $scope.send = function (message) {
            // no validation
            var messageCopy = angular.copy(message);
            messageCopy.timestamp = (new Date());
            messageCopy.from = getUser();
            messageCopy.direction = 'sent';
            chatter.send(messageCopy, $scope.conversation);
            $scope.message.content = '';
        };

        function newMessageCallback(message) {
            $scope.$apply(function () {
                chatter.uniqueUnshift(angular.copy(message), $scope.conversation);
            });
        }

        chatter.registerCB(newMessageCallback);

        function showMessageCB(result) {
            angular.forEach(result, function (mes) {
                chatter.uniquePush(JSON.parse(mes.message), $scope.conversation);
            });
        }
        function showMessageClearCB(result) {
            var newList = []
            angular.forEach(result, function (mes) {
                chatter.uniquePush(JSON.parse(mes.message), newList);
            });
            $scope.$apply(function() {
                $scope.conversation = newList;
            });
        }

        function userListCB() {
            getUserList();
        }

        // to update upon users becoming active or inactive
        chatter.registerUserListCB(userListCB);
        messageService.purgeMessageCount(recipientid);


        function getUserList() {
            console.log('details getUserList');
            var currentUserId = getUser()._id;
            chatter.getUserList(function (usersr, activeUsers) {
                var all = angular.fromJson(usersr);
                $scope.message.recipient = getById(all, recipientid)[0];
                var users = angular.fromJson(usersr);

                // Remove current user as possible recipient
                $scope.users = [];
                angular.forEach(users, function (user) {
                    if (user._id !== currentUserId) {
                        $scope.users.push(user);
                    }
                });

            });
        }

        function getMessages(id, clear) {
            if(clear) {
                databaseService.getNumberOfMessages(5, id, showMessageClearCB);
            } else {
            databaseService.getNumberOfMessages(5, id, showMessageCB);
            }
        }

        getMessages(recipientid);

        //initialize
        console.log('detail intial call out to getUserList');
        getUserList();
    }) // End ChatDetail

    .controller('RegisterCtrl', function ($scope, $state, userService) {
        $scope.user = {};
        $scope.user.password = '';
        $scope.user.name = '';
        $scope.user.email = '';
        function success() {
            $state.go('log.login');
        }

        function failure() {
        }

        $scope.submit = function () {
            console.log("hit conttoller");
            userService.register($scope.user, success, failure);
        }
    })

    .controller('LoginCtrl', function ($scope, $rootScope, $state, userService) {
        $scope.user = {};
        $scope.user.password = '';
        $scope.user.name = '';
        var success = function (user) {
            console.log("Success");
            $rootScope.user = user;
            $state.go('tab.chats');
        };
        $scope.submit = function () {
            userService.login($scope.user, success);
        };
    })
;
